const db = require("../models");
const Customer = db.customer;
const Restaurant = db.restaurant
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Op = db.Sequelize.Op;

// create Id for User
createId = async () => {
  id = Math.floor(Math.random() * 10000) + 1;
  let checkId = await Customer.findOne({ where: { UserId: id } });
  if (checkId === null) {
    console.log(id)
    return id;
  } else {
    createId();
  }
}

// Create and Save a new Customer User.
exports.create = async (req, res) => {
  const Id = await createId();
  const email = req.body.Email
  const username = req.body.Username
  const phone = req.body.Phone
  const c_password = req.body.C_Password
  const Name = req.body.Name
  const name = Name.split(" ");

  bcrypt.hash(req.body.Password, 10).then((hashedPassword) => {
    // Create a Customer
    const customer = {
      UserId: Id,
      Email: email,
      Username: username,
      First_Name: name[0],
      Last_Name: name[1],
      Password: hashedPassword,
      Phone: req.body.Phone,
      Restaurant: req.body.Restaurant ? req.body.Restaurant : false
    };

    bcrypt.compare(c_password, hashedPassword).then((passwordCheck) => {
      if (passwordCheck) {
        const checkEmail = Customer.findOne({ where: { Email: email } })
        if (checkEmail === null) {
          const checkUsername = Customer.findOne({ where: { Username: username } })
          if (checkUsername === null) {
            const checkPhone = Customer.findOne({ where: { Phone: phone } })
            if (checkPhone === null) {
              // Save Customer in the database
              Customer.create(customer)
                .then(data => {
                  return res.status(200).send({
                    message: "User Created Successfully",
                    data
                  });
                })
            } else {
              return res.status(400).send({
                message: "This Phone Number has been used by another account"
              })
            }
          } else {
            return res.status(400).send({
              message: "This Username has been used by another account"
            })
          }
        } else {
          return res.status(400).send({
            message: "This Email has been used by another account"
          })
        }
      } else {
        return res.status(400).send({
          message: "Passwords does not match with Confirm Password"
        });
      }
    })

  })
};

// Create and Save a new Restaurant User.
exports.createrest = async (req, res) => {
  const Id = await createId();
  const c_password = req.body.C_Password
  const Name = req.body.Name
  const name = Name.split(" ");

  bcrypt.hash(req.body.Password, 10).then((hashedPassword) => {
    // Create a Customer
    const customer = {
      UserId: Id,
      Email: req.body.Email,
      Username: req.body.Username,
      First_Name: name[0],
      Last_Name: name[1],
      Password: hashedPassword,
      Phone: req.body.Phone,
      Restaurant: req.body.Restaurant ? req.body.Restaurant : true
    };

    const restaurant = {
      Owner_Id: Id,
      Owner_Email: req.body.Email,
      Owner_Username: req.body.Username,
      Pro_Id: Id,
      Rest_Name: req.body.Rest_Name,
      Time_Open: req.body.Time_Open,
      Time_Close: req.body.Time_Close,
      Rest_Type: req.body.Rest_Type,
      Address: req.body.Address
    }

    bcrypt.compare(c_password, hashedPassword).then((passwordCheck) => {
      if (passwordCheck) {
        const checkEmail = Customer.findOne({ where: { Email: email } })
        if (checkEmail === null) {
          const checkUsername = Customer.findOne({ where: { Username: customer.Username } })
          if (checkUsername === null) {
            const checkPhone = Customer.findOne({ where: { Phone: customer.Phone } })
            if (checkPhone === null) {
              // Save Customer in the database
              Customer.create(customer)
                .then(c_data => {
                  Restaurant.create(restaurant).then(r_data => {
                    return res.status(200).send({
                      message: "User Created Successfully",
                      data: {
                        c_data,
                        r_data
                      }
                    });
                  })
                }).catch((error) => {
                  res.status(500).send({
                    message: error
                  })
                })
            } else {
              return res.status(400).send({
                message: "This Phone Number has been used by another account"
              })
            }
          } else {
            return res.status(400).send({
              message: "This Username has been used by another account"
            })
          }
        } else {
          return res.status(400).send({
            message: "This Email has been used by another account"
          })
        }
      } else {
        return res.status(400).send({
          message: "Passwords does not match with Confirm Password"
        });
      }
    })

  })
};

exports.login = (req, res) => {
  const email = req.body.Email
  const password = req.body.Password

  Customer.findOne({ where: { Email: email } }).then((data) => {
    bcrypt.compare(password, data.Password).then((passwordCheck) => {
      if (!passwordCheck) {
        return res.status(400).send({
          message: "Passwords does not match"
        });
      }
      //   create JWT token
      const token = jwt.sign(
        {
          userId: data.UserId,
        },
        "RANDOM-TOKEN",
        { expiresIn: "24h" }
      );
      return res.status(200).send({
        message: "Login Successful",
        email: data.Email,
        token,
      });
    })
  })// catch error if account do not found
    .catch((error) => {
      res.status(400).send({
        message: "We does not found your account!"
      });
    });
}

exports.userdata = async (req, res) => {
  try {
    const token = await request.headers.authorization.split(" ")[1];
    const decodedToken = await jwt.verify(token, "RANDOM-TOKEN");
    const user = await decodedToken;
    return res.status(200).send({
      user
    })
  } catch (error) {
    return res.status(400).send({
      error: new Error("Invalid request!")
    })
  }
}

// Retrieve all Tutorials from the database.
exports.findAll = (req, res) => {
  const Email = req.query.Email;
  var condition = Email ? { Email: { [Op.like]: `%${Email}%` } } : null;

  Customer.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};


// Find a single Customer with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  Customer.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Customer with id=" + id
      });
    });
};

// Update a Customer by the id in the request
exports.update = (req, res) => {
  Customer.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Customer was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Customer with id=${id}. Maybe Customer was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Customer with id=" + id
      });
    });
};

// Delete a Customer with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  Customer.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Customer was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Customer with id=${id}. Maybe Customer was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Customer with id=" + id
      });
    });
};

// Delete all Customer from the database.
exports.deleteAll = (req, res) => {
  Customer.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Customer were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Customer."
      });
    });
};

// find all Restaurant Customer
exports.findAllRestaurant = (req, res) => {
  Customer.findAll({ where: { Restaurant: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

module.exports = app => {
  const customer = require("../controllers/client.controller.js");

  var router = require("express").Router();

  // Create a new Customer
  router.post("/signup", customer.create);

  // Create a new Customer
  router.post("/signuprest", customer.createrest);

  // Login a Customer
  router.post("/signin", customer.login);

  // Retrieve all Customer
  router.get("/", customer.findAll);

  // Retrieve all Restaurant Customer
  router.get("/restaurant", customer.findAllRestaurant);

  // Retrieve a single Customer with id
  router.get("/:id", customer.findOne);

  // Update a Customer with id
  router.put("/:id", customer.update);

  // Delete a Customer with id
  router.delete("/:id", customer.delete);

  // Delete all Customer
  router.delete("/", customer.deleteAll);

  app.use('/api/customer', router);
};

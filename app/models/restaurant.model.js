module.exports = (sequelize, Sequelize) => {
    const Restaurant = sequelize.define("CafeHub_Restaurant", {
        Owner_Id: {
            type: Sequelize.STRING
        },
        Owner_Email: {
            type: Sequelize.STRING
        },
        Owner_Username: {
            type: Sequelize.STRING
        },
        Pro_Id: {
            type: Sequelize.INTEGER
        },
        Rest_Name: {
            type: Sequelize.CHAR
        },
        Time_Open: {
            type: Sequelize.TIME
        },
        Time_Close: {
            type: Sequelize.TIME
        },
        Rest_Type: {
            type: Sequelize.CHAR
        },
        Address: {
            type: Sequelize.STRING
        }
    });

    return Restaurant;
};

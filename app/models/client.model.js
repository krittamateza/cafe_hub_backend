module.exports = (sequelize, Sequelize) => {
  const Customer = sequelize.define("CafeHub_Customer", {
    UserId:{
      type: Sequelize.INTEGER
    },
    Email: {
      type: Sequelize.STRING
    },
    Username: {
      type: Sequelize.STRING
    },
    First_Name: {
      type: Sequelize.STRING
    },
    Last_Name: {
      type: Sequelize.STRING
    },
    Password: {
      type: Sequelize.STRING
    },
    Phone: {
      type: Sequelize.INTEGER
    },
    Restaurant: {
      type: Sequelize.BOOLEAN
    }
  });

  return Customer;
};

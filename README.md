# Cafe_Hub_BackEnd

## Dependences
- [Express](https://www.npmjs.com/package/express)
- [Node](http://nodejs.org/)
- [bcrypt](https://www.npmjs.com/package/bcrypt)
- [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)
- [sequelize](https://www.npmjs.com/package/sequelize)
- [cors](https://www.npmjs.com/package/cors)
- [mysql2](https://www.npmjs.com/package/mysql2)


## SETTING UP 
- Clone the repositury to your machine
- Open up a terminal
- Navigate into the project directory
- Run <code>npm install</code> to install all needed dependencies
- Run <code>node server.js</code> to spin up the server
- The server runs on port 4000 <code>http://localhost:4000/</code>
